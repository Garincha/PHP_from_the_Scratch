<form action="get_method.php" method="get">
    Name:<br><input type="text" name="name"><br>
    Age: <br><input type="text" name="age"><br><br>
    <input type="submit" value="submit">

</form>

<?php
/**
 * Created by PhpStorm.
 * User: tnt
 * Date: 4/19/2017
 * Time: 7:38 PM
 */
//$_GET is an assosiative array.

$name = $_GET['name'];// here 'name' is working as a key. when we use ' name=dale ' in a url, we can pass value by this key(name).We can also use name key in a form's input type to get value from a user by get method.

$age = $_GET['age'];// here 'age' is working as a key. when we use ' age=34 ' in a url, we can pass value by this key(age).We can also use age key in a form's input type to get value from a user by get method.

if (isset($name) && isset($age)&& !empty($name) &&!empty($age)) {
    echo 'I am ' . $name . ' and my age is ' . $age;
}else{
    echo "Insert correct input.";
}

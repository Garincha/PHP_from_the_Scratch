<?php
/**
 * Created by PhpStorm.
 * User: tnt
 * Date: 6/4/2017
 * Time: 10:54 PM
 */

$time = time();

$time_now = date('d M Y @ H:i:s',$time);
$modified_time = date('d M Y @ H:i:s',$time-60);
$modified_time2 = date('d M y @ H:i:s',strtotime('+1 week 1 hour 1 minute 1 second'));

echo "The current time is ".$time_now." and the modified time is ".$modified_time."<br>";
echo "Showing time using strtotime function ".$modified_time2;
<?php
/**
 * Created by PhpStorm.
 * User: tnt
 * Date: 5/5/2017
 * Time: 12:31 AM
 */
$n1 = 10;
$n2 = 5;

function add($num1, $num2)//here, $num1 & $num2 are parameters.
{
    echo $num1 + $num2;
}

add($n1,$n2);// here $n1 & $n2 are arguments.
echo "<br>";

function display($day,$date,$year)
{
    echo 'Today is '.$day." ".$date." ".$year;
}

display('Sunday',1,2030);
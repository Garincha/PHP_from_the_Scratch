<?php
/**
 * Created by PhpStorm.
 * User: tnt
 * Date: 6/19/2017
 * Time: 11:48 AM
 */

class myclass{
    public $age;//this is a property without fixed value.
    public $name = 'garincha';// this is a property with a fixed value, it is called property initialization.
    //public, protected, private... these are visibility keyword.
}

$person = new myclass();
$person->age = 45;// -> the name of this sign is object operator. we are accessing the property age through this ->(object operator).

$another_person = new myclass();
$another_person->age = 35;

var_dump($person->age);
echo "<br>";
var_dump($another_person->age);
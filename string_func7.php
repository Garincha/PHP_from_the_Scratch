<?php
/**
 * Created by PhpStorm.
 * User: tnt
 * Date: 5/21/2017
 * Time: 7:52 PM
 */

$string = " This is an example of string ";
$string_trimmed = trim($string);
echo $string."<br>";
echo $string_trimmed."<br>";
$string_ltrimmed = ltrim($string);
echo $string_ltrimmed."<br>";
$string_rtrimmed = rtrim($string);
echo $string_rtrimmed;
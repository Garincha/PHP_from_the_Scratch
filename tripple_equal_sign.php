<?php
/**
 * Created by PhpStorm.
 * User: tnt
 * Date: 4/9/2017
 * Time: 11:23 PM
 */

$num1 = 20;
$num2 = '20';

if ($num1 == $num2){// "==" not compares the data type of both variable,only compares the value.
    echo "Equal"."<br>";
}else{
    echo "Not Equal"."<br>";
}

if ($num1 === $num2){//"===" compares the data type of both variable with their value.
    echo "Equal"."<br>";
}else{
    echo "Not Equal.";
}
<?php
/**
 * Created by PhpStorm.
 * User: tnt
 * Date: 5/19/2017
 * Time: 12:09 AM
 */

$str = "I am a php developer .";
$string_word_count = str_word_count($str);// counting the total words of the variable $str.
echo $string_word_count."<br>";
$string_word_count2 = str_word_count($str,1);// because of the argument 1, the value will be stored in an indexed array. we need to use print_r().
print_r($string_word_count2);
$string_word_count3 = str_word_count($str,2);// because of the argument 2, the value will be stored in an associative array.
print_r($string_word_count3);
$string_word_count4 = str_word_count($str,1,'.');// by third argument, we can add the full stop inside the indexed array.
print_r($string_word_count4);
<?php
/**
 * Created by PhpStorm.
 * User: tnt
 * Date: 5/3/2017
 * Time: 8:19 PM
 */

$month = 'January';
$date = 31;
$year = 2017;

echo 'Today is '.$date.' '.$month.' '.$year."<br>";
echo 'Today is <strong>'.$date.' '.$month.' '.$year.'</strong>'."<br>";
echo 'Today is $date $month $year'."<br>";// this line with single quotation won't show the value assigned in those three valiables.
echo "Today is $date $month $year"."<br>";// this line with double quotation will show the value assigned in those three variables.



<?php
/**
 * Created by PhpStorm.
 * User: tnt
 * Date: 6/23/2017
 * Time: 10:51 AM
 */

trait CommonMethods{
    public function getName(){
        return $this->name;
    }
}

class TestClass{
    use CommonMethods;
    private $name = "test class";
}

class AnotherClass{
    use CommonMethods;
    private $name = "another class";
}

$obj = new TestClass();
var_dump($obj->getName());

$obj2 = new AnotherClass();
var_dump($obj2->getName());
<?php
/**
 * Created by PhpStorm.
 * User: tnt
 * Date: 6/22/2017
 * Time: 12:31 AM
 */

class myClass{
    public $pro = "Public";
    protected $pro2 = "Protected";
    private $pro3 = "Private";

    function myFunction(){
        echo $this->pro;
        echo $this->pro2;
        echo $this->pro3;
    }
}

$obj = new myclass();
$obj->myFunction();
echo "<br>";
echo $obj->pro;
echo $obj->pro2;
echo $obj->pro3;

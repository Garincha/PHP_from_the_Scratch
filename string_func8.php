<?php
/**
 * Created by PhpStorm.
 * User: tnt
 * Date: 5/21/2017
 * Time: 8:19 PM
 */

$string = "This is a <img src='image.jpeg'>string";
$string_slashes = htmlentities(addslashes($string));
echo $string_slashes."<br>";
echo stripcslashes($string_slashes);
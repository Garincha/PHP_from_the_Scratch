<?php
/**
 * Created by PhpStorm.
 * User: tnt
 * Date: 4/6/2017
 * Time: 8:28 PM
 */

// ==(equal to), !=(not equal to), >(greater than), <(less than), >=(greater than or equal to), <=(less than or equal to),these are comparison operators.

$name = "Kaka";
$age = 25;
$status = false;
if($name == "Kaka"){
    echo "OK."."<br>";
}elseif ($name != "Kaka"){
    echo "Wrong"."<br>";
}

if ($age >= 25){
    echo "Right"."<br>";
}elseif ($age <= 24){
    echo "Not matching"."<br>";
}

if($age >= 25){
    $status = true;
}
if ($status == true){
    echo "It's True."."<br>";
}elseif ($status == false){
    echo "It's False.";
}

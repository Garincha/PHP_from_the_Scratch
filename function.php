<?php
/**
 * Created by PhpStorm.
 * User: tnt
 * Date: 4/14/2017
 * Time: 11:45 PM
 */

function abc(){
    echo 'Hypertext Pre Processor'.'<br>';
}

abc();

function name($a){
    echo $a.'<br>';
}

name('Sam');

function name2($b){
    return $b.'<br>';
}

$returnvalue = name2('Billy');

echo $returnvalue;

function lmg(){
    return 'Henry'.'<br>';
}

$show = lmg();

echo $show;

function tnt($name,$age){
    return 'My Name is '.$name.' and age is '.$age;
}

$aaa = tnt('Valintina','45');

echo $aaa."<br>";

function add($num1,$num2){
    $result = $num1 + $num2;//keeping the values(coming through arguments) in a variable.
    return $result;//returning that variable
}

echo add(12,34);// now we can directly echo that function add, cause within that function we returned the value(passing through arguments) in a variable.
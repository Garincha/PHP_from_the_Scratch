<?php
/**
 * Created by PhpStorm.
 * User: tnt
 * Date: 4/18/2017
 * Time: 12:22 AM
 */

function add($num1,$num2){
    $total = $num1 + $num2;
    return $total;
}

echo add(13,23)."<br>";

function plus(){
    $total = 0;
    print_r(func_get_args());// this will show an array with the values coming through arguments.
}

echo plus(12,4,1)."<br>";

function plus2(){
    $total = 0;
    foreach (func_get_args() as $arg){
        $total = $total + $arg;
    }
    return $total;
}

echo plus2(10,10,10,10,10)."<br>";

function append($initial,$int2){
    $result = func_get_arg(0);
    foreach (func_get_args() as $key => $values){
        if ($key >= 1){
                $result .= ''.$values;
        }
    }
    return $result;
}

echo append('alex',' marphy');//using string as arguments value.
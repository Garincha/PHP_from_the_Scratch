<?php
/**
 * Created by PhpStorm.
 * User: tnt
 * Date: 6/27/2017
 * Time: 1:15 PM
 */

class Model
{
    protected $observers;

    public function __construct()
    {
        $this->observers = new SplObjectStorage();
    }

    public function notify()
    {
        foreach ($this->observers as $observer){
            $observer->update($this);
        }
    }

    public function setObservers($observers = [])
    {
        foreach ($observers as $observer){
            $this->observers->attach($observer);
        }
    }
    public function __set($name, $value)
    {
        // TODO: Implement __set() method.
        $this->data[$name] = $value;
        //notify the observers, that model has been updated
        $this->notify();
    }
}
class Post extends Model
{
    public function insert($data)
    {
        $this->notify();
    }

    public function update($data)
    {
        $this->notify();
    }
    public function delete($id)
    {
        $this->notify();
    }
}
class PostModelObserver
{
    public function update($subject)
    {
        echo get_class($subject).' has been updated'.'<br>';
    }
}

class Observer2
{
    public function update($subject)
    {
        echo get_class($subject).' has been updated'.'<br>';
    }
}

$post = new Post();

$post->setObservers([new PostModelObserver, new Observer2]);

$post->title = "Hello World";
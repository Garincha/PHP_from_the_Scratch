<?php
/**
 * Created by PhpStorm.
 * User: tnt
 * Date: 6/21/2017
 * Time: 12:58 AM
 */

class Person{
    public static $count;

    public static function getCount(){
        return self::$count;//self is working here as a $this variable.we're accessing value by itself.
    }// here, in static context we're using ::(this sign) instead of ->(object operator,which is used in non static context) to access value.
}   // the sign :: is called scope resoulation operator.

Person::$count = 34;
var_dump(Person::$count);

$person = new Person();
var_dump($person->getCount());

Person::$count = 23;
var_dump(Person::$count);

var_dump($person->getCount());

$person2 = new Person();
var_dump($person2->getCount());


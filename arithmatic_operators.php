<?php
/**
 * Created by PhpStorm.
 * User: tnt
 * Date: 4/6/2017
 * Time: 7:31 PM
 */

// +, -, *, /, %, ++, -- these all are arithmatic operators.

$num1 = 20;
$num2 = 10;

$result = $num1 + $num2;
echo $result."<br>";
$result2 = $num1 - $num2;
echo $result2."<br>";
$result3 = $num1 * $num2;
echo $result3."<br>";
$result4 = $num1 / $num2;
echo $result4."<br>";
$result5 = $num1 % $num2;
echo $result5."<br>";
$num1++;
echo $num1."<br>";
$num2--;
echo $num2;
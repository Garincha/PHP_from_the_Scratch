<?php
/**
 * Created by PhpStorm.
 * User: tnt
 * Date: 4/26/2017
 * Time: 12:06 AM
 */

$mmd = array( // this is an example of multi dimensonial array


    1 => array('name' => 'Aronomix','age' => 33, 'profession' => 'Teacher'),
    2 => array('name' => 'Hammershield','age' => 33, 'profession' => 'Secretary'),
    3 => array('name' => 'Nedved','age' => 33, 'profession' => 'striker')
);

echo $mmd."<br>";
echo $mmd[1]['name']."<br>";
echo $mmd[2]['age']."<br>";
echo $mmd[3]['profession'];
$mmd[4] = array('name' => 'Harris','age' => 45,'profession' => 'stoppers');// inserting an element seperately from ouside of $mmd array.
echo '<pre>',print_r($mmd,true),'</pre>';// pre tag is working here to print out the array, line by line.

$GLOBALS = array(// it's a three dimensonal array,first array is $GOLBALS.


    1 => array('name' => 'Aronomix','age' => '33', 'profession' => 'Teacher'),
    2 => array('name' => 'Hammershield','age' => '33', 'profession' => 'Secretary'),
    3 => array('name' => 'Nedved','age' => '33', 'profession' => 'striker')
);
echo $GLOBALS[1]['profession'];
echo "<br>";

function show_data($element,$value){// using this function we are getting a exact part of the above array
        if (array_key_exists($element,$GLOBALS)===true){//the function array_key_exists checking the 1st parameter $element against the 2nd parameter $GOBALS.
                return $GLOBALS[$element][$value];
        }else{
            return false;
        }
}
echo show_data(3,'age');

echo '<pre>',print_r($GLOBALS,true),'</pre>';
echo "<br>";

print_r($GLOBALS);
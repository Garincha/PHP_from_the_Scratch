<?php
/**
 * Created by PhpStorm.
 * User: tnt
 * Date: 6/1/2017
 * Time: 11:31 PM
 */
$offset = 0;
if (isset($_POST['text'])&&isset($_POST['search'])&&isset($_POST['replace'])){
    $text = $_POST['text'];
    $search = $_POST['search'];
    $replace = $_POST['replace'];
    $search_length = strlen($search);
    if (!empty($text)&&!empty($search)&&!empty($replace)){
        while ($strpos = strpos($text,$search,$offset));{
           $offset = $strpos + $search_length;
           $text = substr_replace($text,$replace,$strpos,$search_length);
        }
        echo $text;
    }else{
        echo "Please fill in all the fields.";
    }
}
?>

<form action="find_&_replace_application.php" method="post">
    <textarea rows="6" cols="30" name="text"></textarea><br><br>
    Search for:<br>
    <input type="text" name="search"><br><br>
    Replace With:<br>
    <input type="text" name="replace"><br><br>
    <input type="submit" value="Find and Replace">
</form>

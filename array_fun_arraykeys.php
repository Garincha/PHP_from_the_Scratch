<?php
/**
 * Created by PhpStorm.
 * User: tnt
 * Date: 6/13/2017
 * Time: 11:17 PM
 */

$arr = [1,'fresh','123'];
print_r(array_keys($arr));
echo "<br>";
$arr2 = [1000,'one'=>'Simonds','two'=>56];
print_r(array_keys($arr2));
echo "<br>";
$arr3 = ['morning'=>'breakfast','afternoon'=>'lunch','night'=>'dinner'];
print_r(array_keys($arr3));
echo "<br>";
print_r(array_keys($arr3,'dinner'));
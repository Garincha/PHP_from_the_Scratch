<?php
/**
 * Created by PhpStorm.
 * User: tnt
 * Date: 6/30/2017
 * Time: 6:10 PM
 */

class CarFactory
{
    protected $brands = [];

    public function __construct()
    {
        $this->brands = [
            'mercedes' => 'MercedesCar',
            'toyota' => 'ToyotaCar',
        ];
    }

    public function make($brand)
    {
        if (!array_key_exists($brand,$this->brands)){
            return new Exception('Not available this car');
        }
        $className = $this->brands[$brand];

        return new $className();
    }
}

interface CarInterface
{
    public function design();
    public function assemble();
    public function paint();
}
class MercedesCar implements CarInterface
{
    public function design()
    {
        // TODO: Implement design() method.
        return 'Designing Mercedes Car';
    }

    public function assemble()
    {
        // TODO: Implement assemble() method.
        return 'Assembling Mercedes Car';
    }
    public function paint()
    {
        // TODO: Implement paint() method.
        return 'Painting Mercedes Car';
    }
}
class ToyotaCar implements CarInterface
{
    public function design()
    {
        // TODO: Implement design() method.
        return 'Designing Toyota Car';
    }
    public function assemble()
    {
        // TODO: Implement assemble() method.
        return 'Assembling Toyota Car';
    }
    public function paint()
    {
        // TODO: Implement paint() method.
        return 'Painting Toyota Car';
    }
}
$carFactory = new CarFactory;

$mercedes = $carFactory->make('mercedes');
echo $mercedes->design().'<br>';
echo $mercedes->assemble().'<br>';
echo $mercedes->paint().'<br>';

echo '<br>';

$toyota = $carFactory->make('toyota');
echo $toyota->design().'<br>';
echo $toyota->assemble().'<br>';
echo $toyota->paint().'<br>';

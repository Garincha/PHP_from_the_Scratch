<?php
/**
 * Created by PhpStorm.
 * User: tnt
 * Date: 4/19/2017
 * Time: 1:13 PM
 */

$num = 12894934;

echo "I have &pound;".number_format($num)."<br>";//nuber_format function displays the number with right format.

$num2 = 49834398.8550;

echo "The whole amount is ".number_format($num2,4)."<br>";

$num3 = 239329.8488949;

echo "The actual number is ".number_format($num3,5,'.',',');
<?php
/**
 * Created by PhpStorm.
 * User: tnt
 * Date: 6/11/2017
 * Time: 12:31 AM
 */

function foo() {
    echo "In foo()<br />\n";
}
function bar($arg = '')
{
    echo "In bar(); argument was '$arg'.<br />\n";
}
// This is a wrapper function around echo
function echoit($string)
{
    echo $string;
}
$func = 'foo';
$func(); // This calls foo()
$func2 = 'bar';
$func2('test'); // This calls bar()
$func3 = 'echoit';
$func3('test'); // This calls echoit()
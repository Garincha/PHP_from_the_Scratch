<?php
/**
 * Created by PhpStorm.
 * User: tnt
 * Date: 5/28/2017
 * Time: 1:43 PM
 */

$string = "This is a string, and it is an example";

$find = ['is','a','example'];//by this array, we can find the targated string from the variable.
$replace = ['IS','A','EXAMPLE'];//by this array, we can replace the targated string in the variable.

$new_string = str_replace($find,$replace,$string);

echo $new_string;
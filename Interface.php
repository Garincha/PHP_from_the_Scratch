<?php
/**
 * Created by PhpStorm.
 * User: tnt
 * Date: 6/23/2017
 * Time: 12:14 AM
 */

interface iTemplate//declare the interface as iTemplate
{
    public function setVariable($name,$var);
    public function getHtml($template);
}

class Template implements iTemplate
{
    private $vars = array();

    public function setVariable($name, $var)
    {
        // TODO: Implement setVariable() method.
        $this->vars[$name] = $var;
    }
    public function getHtml($template)
    {
        // TODO: Implement getHtml() method.
        foreach ($this->vars as $name => $value){
            $template = str_replace('{'.$name.'}',$value,$template);
        }
        return $template;
    }
}
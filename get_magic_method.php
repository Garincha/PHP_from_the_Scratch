<?php
/**
 * Created by PhpStorm.
 * User: tnt
 * Date: 6/23/2017
 * Time: 11:38 AM
 */

class TestGet{
    public $username = "Kenndy bush";
    private $age  = 35;
    public function __get($age)//purpose of the get method, to access the value of any property of a class.
    {
        // TODO: Implement __get() method.
        if (property_exists($this,$age)){
            return $this->age;
        }
    }
}

$obj = new TestGet();
echo $obj->age;
<?php
/**
 * Created by PhpStorm.
 * User: tnt
 * Date: 6/23/2017
 * Time: 8:58 PM
 */

class CheckIsset{
    public $name = "Solanki";
    public function __isset($name)//the purpose of isset method, check the property is set or not.
    {
        // TODO: Implement __isset() method.
        return isset($this->name);
    }

}

$obj = new CheckIsset();
if (isset($obj->name)){
    echo "The defined property is set";//if the property is set, the phrase will show on the browser.
}
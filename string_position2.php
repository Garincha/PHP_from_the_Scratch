<?php
/**
 * Created by PhpStorm.
 * User: tnt
 * Date: 5/27/2017
 * Time: 7:51 PM
 */

$string = "This is an string, and it is an example";

$find = 'is';

$find_length = strlen($find);
$offset = 0;

while ($string_position = strpos($string,$find,$offset)){
    echo $find.' found at '.$string_position."<br>";
    $offset = $string_position + $find_length;
}
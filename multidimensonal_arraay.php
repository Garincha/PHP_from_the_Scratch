<?php
/**
 * Created by PhpStorm.
 * User: tnt
 * Date: 4/11/2017
 * Time: 8:38 PM
 */

$names = array(
    'John'=>array('Age'=>30,'Height'=>6.2,'Food'=>array('Chicken','Pizza')),
    'Harris'=>array('Age'=>35,'height'=>6),
    'Peter'=>array('Age'=>45,'height'=>6.5),
);

print_r($names);//prints the whole array
echo "<br>";
echo $names['John']['Food'][0]."<br>";//prints the specific part of the $names array
echo $names['Peter']['Age']."<br>";//prints the specific part of the $names array.
print_r($names['Harris']['height']);//prints the specific part of the $names array.
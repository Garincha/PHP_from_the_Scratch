<?php
/**
 * Created by PhpStorm.
 * User: tnt
 * Date: 6/22/2017
 * Time: 11:58 PM
 */

class destroyClass{
    function __construct()
    {
        echo "Welcome from magic method"."<br>";
        $this->name = "destroyClass";
    }
    function __destruct()
    {
        echo "Destroying ".$this->name."\n";
    }
}
$obj = new destroyClass();
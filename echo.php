<?php
/**
 * Created by PhpStorm.
 * User: tnt
 * Date: 4/5/2017
 * Time: 8:50 PM
 */

echo 'Hello Everyone'.'<br>';// example of single quotation
//echo 'Hello World. It's a lovely day.'<br>' ;// disadvantages of single quotation
echo 'Hello World. It\'s lovely day.'.'<br>';// disadvantages removed by a back slash.
echo "Hello world. It's my drilling with PHP.";// example of double quotation.
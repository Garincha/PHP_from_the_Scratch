<?php
/**
 * Created by PhpStorm.
 * User: tnt
 * Date: 6/23/2017
 * Time: 1:43 PM
 */

class SetClass{
    public $name ;

    public function __set($name, $value)//the purpose of set method, to set a value in a class property
    {
        // TODO: Implement __set() method.
        if (property_exists($this,$name)){
            $this->name = $value;
        }
    }
}

$obj = new SetClass();
$obj->name = "Amarant";//setting the value in $name property.
echo $obj->name;
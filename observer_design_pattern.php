<?php
/**
 * Created by PhpStorm.
 * User: tnt
 * Date: 6/27/2017
 * Time: 12:24 PM
 */

class Model implements SplSubject
{
    protected $observers;

    public function __construct()
    {
        $this->observers = new SplObjectStorage();
    }

    public function attach(SplObserver $observer)
    {
        // TODO: Implement attach() method.
        $this->observers->attach($observer);
    }
    public function detach(SplObserver $observer)
    {
        // TODO: Implement detach() method.
        $this->observers->detach($observer);
    }
    public function notify()
    {
        // TODO: Implement notify() method.
        foreach ($this->observers as $observer){
            $observer->update($this);
        }
    }
    public function __set($name, $value)
    {
        // TODO: Implement __set() method.
        $this->data[$name] = $value;
        $this->notify();//notify the observers, that model has been updated
    }
}

class ModelObserver implements SplObserver
{
    public function update(SplSubject $subject)
    {
        // TODO: Implement update() method.
        echo get_class($subject).' has been updated'.'<br>';
    }
}
class Observer2 implements SplObserver
{
    public function update(SplSubject $subject)
    {
        // TODO: Implement update() method.
        echo get_class($subject).' has been updated'.'<br>';
    }
}
//Instantiate the model class for 2 different objects
$model1 = new Model();
$model2 = new Model();
//Instantiate the observers
$modelObserver = new ModelObserver();
$observer2 = new Observer2();
//Attach the observers to $model1
$model1->attach($modelObserver);
$model1->attach($observer2);
//Attach the observers to $model2
$model2->attach($observer2);
//changing the subject properties
$model1->title = 'Hello World';
$model2->body = 'Lorem ipsum........';
<?php
/**
 * Created by PhpStorm.
 * User: tnt
 * Date: 6/22/2017
 * Time: 12:16 AM
 */

class ParentClass{
    public $age;

    public function getName(){
        return $this->age;
    }
}

class SubClass extends ParentClass{
    //defined nothing, inheriting all property & method of parent class.
}

$obj = new SubClass();
$obj->age = 24;
var_dump($obj->getName());
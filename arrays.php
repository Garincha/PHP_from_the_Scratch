<?php
/**
 * Created by PhpStorm.
 * User: tnt
 * Date: 4/11/2017
 * Time: 7:54 PM
 */

$names = "Alex(24),Bobby(34),Glenn(49)";//this isn't an array.

echo $names."<br>";

$names2 = array('Sam','John','Kenndy');//here, key is 0 & its value is Sam, key is 1 & its value is John, key is 2 & its value is Kenndy.

print_r($names2)."<br>" ;

$names3 = array('Andy' => 27,'Tony' => 34,'Emmie' => 28);//here, Andy is key and its value is 27.

print_r($names3);


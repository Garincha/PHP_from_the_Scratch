<?php
/**
 * Created by PhpStorm.
 * User: tnt
 * Date: 6/11/2017
 * Time: 10:21 AM
 */

function fact($n) {
    if ($n === 0) { // our base case
        return 1;
    }
    else {
        return $n * fact($n-1); // <--calling itself.
    }
}
var_dump(fact(10));
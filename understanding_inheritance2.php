<?php
/**
 * Created by PhpStorm.
 * User: tnt
 * Date: 6/22/2017
 * Time: 12:23 AM
 */

class First{
    public $name = "Hi, I'm from the first class.";//initialised the property $name by assigning value.

    public function getName(){
        return $this->name;
    }
}

class Second extends First{

}

$obj = new Second();
var_dump($obj->getName());
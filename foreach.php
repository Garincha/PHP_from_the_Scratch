<?php
/**
 * Created by PhpStorm.
 * User: tnt
 * Date: 4/13/2017
 * Time: 8:35 PM
 */
$names = array('Jennifer','Maira','Mary');

foreach ($names as $girls){
    echo $girls."<br>";
}

$man = array('Kane','Samuel','Richard');
$num = 1;
foreach ($man as $person){
    echo 'The number '.$num.' is '.$person."<br>";
    $num++;
}

$woman = array('Shimla'=> 25,'Sharon'=> 32,'Annie'=> 36);
foreach ($woman as $model){
    echo $model."<br>";//by echo, we can only print the values.
}

foreach ($woman as $key => $value){//here the key is Shimla & the value is 25.
    echo $key.' is '.$value."<br>";
}
<?php
/**
 * Created by PhpStorm.
 * User: tnt
 * Date: 4/10/2017
 * Time: 11:54 PM
 */

// AND(&&), OR (||), NOT(!) these are logical operators

 $num = 100;

 if ($num >= 1 && $num <= 100){
     echo "The number is within range."."<br>";
 }else{
     echo "The number is out of range."."<br>";
 }

$num2 = 200;

if ($num2 >= 1 && $num2 <= 100){
    echo "The number is within range."."<br>";
}else{
    echo "The number is out of range."."<br>";
}

$name = "Ronald";

if ($name == "Ronald" || $name == "Sam"){
    echo "You're Welcome"."<br>";
}else{
    echo "Sorry. You're not recognised"."<br>";
}

$name2 = "Sandy";
$age = 24;
if($name2 == "Sandy" && ($age = 20 || $age = 24)){
    echo "Hi there!"."<br>";
}else{
    echo "Sorry, It's not you.";
}
<?php
/**
 * Created by PhpStorm.
 * User: tnt
 * Date: 4/6/2017
 * Time: 11:48 AM
 */

$name = 'Sam';
$age = 25;
$dog = "Packy";
echo 'My name is '.$name.' and my age is '.$age.'<br>';//echo with single quotation
echo "My name is $name and my age is $age"."<br>";//echo with double quotation
echo "My pet dog's name is $dog";
<?php
/**
 * Created by PhpStorm.
 * User: tnt
 * Date: 6/22/2017
 * Time: 11:42 PM
 */

class FirstClass{
    function __construct()
    {
        echo "I'm from parent class.";
    }
}
$obj = new FirstClass();
echo "<br>";
class SecondClass extends FirstClass{
    function __construct()
    {
        parent::__construct();//calling the parents class construct method
        print "Inheritated by subclass";
    }
}

$obj2 = new SecondClass();
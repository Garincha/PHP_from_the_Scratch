<?php
/**
 * Created by PhpStorm.
 * User: tnt
 * Date: 6/21/2017
 * Time: 12:23 AM
 */

class person{
    public $age;
    public $name;

    public function getAge(){//getAge is a method.
        return $this->age;//by $this variable, we are accessing the value of property $age in this method. values are assigned through the object.
        // $this is a non static context.
    }

    public function getName(){
        return $this->name;
    }
}

$ob1 = new person();
$ob1->age = 32;

$ob2 = new person();
$ob2->age = 35;

$ob3 = new person();
$ob3->name = 'Bahuballi';

var_dump($ob1->getAge());//because of $this variable(by whom we returned the value inside getAge method), we're getting the value assigned by $ob1.
echo "<br>";
var_dump($ob2->getAge());//because of $this variable(by whom we returned the value inside getAge method), we're getting the value assigned by $ob2.
echo "<br>";
var_dump($ob3->getName());


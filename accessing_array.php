<?php
/**
 * Created by PhpStorm.
 * User: tnt
 * Date: 6/11/2017
 * Time: 10:55 AM
 */

$array = array(
    "foo" => "bar",
    42 => 24,
    "multi" => array(
        "dimensional" => array(
            "array" => "foo"
        )
    )
);
var_dump($array["foo"]);
echo "<br>";
var_dump($array[42]);
echo "<br>";
var_dump($array["multi"]["dimensional"]["array"]);
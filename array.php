<?php
/**
 * Created by PhpStorm.
 * User: tnt
 * Date: 4/25/2017
 * Time: 12:22 PM
 */

$smd = array(34,949,'total');// this is an example of indexed array.

echo $smd."<br>";

var_dump($smd);
echo "<br>";
print_r($smd);
echo "<br>";

$amd = array(     // this is an example of associative array.
    1 => "Level1",//when we want to assign a value(exe - level1) in a element(exe - 1) of an array, we use =>(this sign).
    2 => "Level2",
    3 => "Level3"
);

print_r($amd);
echo "<br>";

$mmd = array( // this is an example of multi dimensonial array


    1 => array('name' => 'Aronomix','age' => '33', 'profession' => 'Teacher'),
    2 => array('name' => 'Hammershield','age' => '33', 'profession' => 'Secretary'),
    3 => array('name' => 'Nedved','age' => '33', 'profession' => 'striker')
);

echo '<pre>',print_r($mmd,true),'</pre>';// pre tag is working here to print out the array, line by line.
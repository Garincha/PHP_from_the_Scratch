<?php
/**
 * Created by PhpStorm.
 * User: tnt
 * Date: 6/23/2017
 * Time: 12:43 AM
 */

abstract class AbstractClass
{
    abstract protected function getValue();
    abstract protected function prefixValue($prefix);

    public function printout(){
        echo $this->getValue()."\n";
    }
}

class ConcreteClass1 extends AbstractClass
{
    protected function getValue()
    {
        // TODO: Implement getValue() method.
        return "ConcreteClass1";
    }
    public function prefixValue($prefix)
    {
        // TODO: Implement prefixValue() method.
        return "{$prefix}ConcreteClass1";
    }

}
class ConcreteClass2 extends AbstractClass
{
    public function getValue()
    {
        // TODO: Implement getValue() method.
        return "ConcreteClass2";
    }
    public function prefixValue($prefix)
    {
        // TODO: Implement prefixValue() method.
        return "{$prefix}ConcreteClass2";
    }
}

$class1 = new ConcreteClass1;
$class1->printout();
echo $class1->prefixValue('FOO_')."\n";

$class2 = new ConcreteClass2;
$class2->printout();
echo $class2->prefixValue('FOO_')."\n";
<?php
/**
 * Created by PhpStorm.
 * User: tnt
 * Date: 6/22/2017
 * Time: 12:51 AM
 */

class Hana{
    public $pub = "Public";//var keyword can be used instead of public keyword.
    protected $pub2 = "Protected";
    private $pub3 = "Private";

    function showVisiblity(){//although missed visiblity keyword, it is a public method.
        echo $this->pub;
        echo $this->pub2;
        echo $this->pub3;
    }
}

class Dul extends Hana{
    function secondTime(){
        echo $this->pub;
        echo $this->pub2;
        echo $this->pub3;//not possible to call/define private property of parent class.
    }
}
$obj = new Dul();
$obj->showVisiblity();
echo "<br>";
echo $obj->pub."<br>";
$obj->secondTime();
echo $obj->pub2;//not possible to call/define protected property of parent class.
echo $obj->pub3;//not possible to call/define private property of parent class.